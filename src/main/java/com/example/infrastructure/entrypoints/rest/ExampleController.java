package com.example.infrastructure.entrypoints.rest;

import com.example.domain.entities.ValidateDate;
import com.example.domain.services.DateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class ExampleController {
    @Autowired
    private DateService dateService;

    @GetMapping("/dates/{startDate}/{endDate}")
    public ResponseEntity<ValidateDate> validateDate(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate) {
        System.out.println(startDate);
        ValidateDate validateDate = dateService.validateDate(startDate, endDate);
        if(validateDate.isValid()) {
            return ResponseEntity.status(HttpStatus.OK).body(validateDate);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validateDate);
        }
    }
}
