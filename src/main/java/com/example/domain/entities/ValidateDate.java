package com.example.domain.entities;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ValidateDate {
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean valid;
}
