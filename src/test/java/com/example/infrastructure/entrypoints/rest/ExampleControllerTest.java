package com.example.infrastructure.entrypoints.rest;

import com.example.domain.entities.ValidateDate;
import com.example.domain.services.DateService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ExampleController.class)
public class ExampleControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DateService dateService;

    @Test
    public void testValidateDate() throws Exception {
        ValidateDate validateDate = new ValidateDate();
        validateDate.setValid(true);
        Mockito.when(dateService.validateDate(Mockito.anyString(), Mockito.anyString())).thenReturn(validateDate);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/test/dates/2020-01-01T00:00:00/2020-01-01T00:00:00");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        Assert.assertEquals("Valid dates", HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    public void testValidateDateOmvaçod() throws Exception {
        ValidateDate validateDate = new ValidateDate();
        validateDate.setValid(false);
        Mockito.when(dateService.validateDate(Mockito.anyString(), Mockito.anyString())).thenReturn(validateDate);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/test/dates/2020-01-01T00:00:00/2020-01-01T00:00:00");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        Assert.assertEquals("Valid dates", HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }
}
