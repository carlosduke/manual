package com.example.domain.services;

import com.example.domain.entities.ValidateDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
@Service
public class DateService {
    private LocalDateTime parseDate(String date){
        if(date == null || date.isEmpty()) {
            return null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            return dateFormat.parse(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        } catch (ParseException e) {
            log.error("Error to parse date", e);
            return null;
        }
    }

    public ValidateDate validateDate(String startDateStr, String endDateStr) {
        LocalDateTime startDate = parseDate(startDateStr);
        LocalDateTime endDate = parseDate(endDateStr);

        ValidateDate validateDate;
        if(startDate == null && endDate == null) {
            validateDate = new ValidateDate();
            validateDate.setValid(false);
        } else if(endDate == null) {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setValid(false);
        } else if(startDate == null) {
            validateDate = new ValidateDate();
            validateDate.setEndDate(endDate);
            validateDate.setValid(false);
        } else if (startDate.isAfter(endDate)) {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setEndDate(endDate);
            validateDate.setValid(false);
        } else {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setEndDate(endDate);
            validateDate.setValid(true);
        }
        return validateDate;
    }
}
