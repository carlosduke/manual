package com.example.domain.services;

import com.example.domain.entities.ValidateDate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Import(DateService.class)
public class DateServiceTest {
    /*
    public ValidateDate validateDate(String startDateStr, String endDateStr) {
        LocalDateTime startDate = parseDate(startDateStr);
        LocalDateTime endDate = parseDate(endDateStr);

        ValidateDate validateDate;
        if(startDate == null || endDate == null) {
            validateDate = new ValidateDate();
            validateDate.setValid(false);
        } else if(endDateStr == null) {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setValid(false);
        } else if(startDateStr == null) {
            validateDate = new ValidateDate();
            validateDate.setEndDate(endDate);
            validateDate.setValid(false);
        } else if (startDate.isAfter(endDate)) {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setEndDate(endDate);
            validateDate.setValid(false);
        } else {
            validateDate = new ValidateDate();
            validateDate.setStartDate(startDate);
            validateDate.setEndDate(endDate);
            validateDate.setValid(true);
        }
        return validateDate;
    }
     */

    @Autowired
    private DateService dateService;

    @Test
    public void validateDateTest(){
        ValidateDate validateDateNull = dateService.validateDate(null,null);
        Assert.assertFalse("Null requests", validateDateNull.isValid());

        ValidateDate validateDateEmpty = dateService.validateDate("","");
        Assert.assertFalse("Empty requests", validateDateEmpty.isValid());

        ValidateDate validateDateParse = dateService.validateDate("invalid","invalid");
        Assert.assertFalse("Invalid requests", validateDateParse.isValid());

        ValidateDate validateDateInvalidStart = dateService.validateDate(null,"2020-01-01T00:00:00");
        Assert.assertFalse("Invalid Start", validateDateInvalidStart.isValid());

        ValidateDate validateDateInvalidEnd = dateService.validateDate("2020-01-01T00:00:00", null);
        Assert.assertFalse("Invalid End", validateDateInvalidEnd.isValid());

        ValidateDate validateDateInvalidDates = dateService.validateDate("2020-01-02T00:00:00", "2020-01-01T00:00:00");
        Assert.assertFalse("Invalid Dates", validateDateInvalidDates.isValid());

        ValidateDate validateDateValid = dateService.validateDate("2020-01-01T00:00:00", "2020-01-01T00:00:00");
        Assert.assertTrue("Invalid Dates", validateDateValid.isValid());
    }
}
